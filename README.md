# Docker containers templates

This is a collection of different containers designed to be run locally for quickly testing various functionalities.

## Contents:

1. MongoDB
2. MinIO

Feel free to explore and experiment with these containers to test and develop functionalities in a local environment.

Happy coding!


