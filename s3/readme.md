# Docker MinIO 

This repository provides a simple example of using Docker Compose to run a MinIO server container. You can customize the environment variables by creating a `.env` file in the project root.

## Prerequisites

- Docker installed on your machine.
- Docker Compose installed on your machine.

## Getting Started

Step - run containers.
at the root of the directory, run : docker-compose up -d

Step - check everything is correct and give it a try
by using the web interface (http://localhost:9001 if you runned the containers on your computer) you should be able to add buckets 